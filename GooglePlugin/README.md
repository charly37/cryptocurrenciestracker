### Installation ###

Just install it from the chrome store for Google document:  https://chrome.google.com/webstore/detail/cryptocurrenciestracker/obhepmncclnmamjanalcifcleiijbdgn?utm_source=permalink

### Usage ###

To use it just click on any cell and use the formula GET_CRYPTO(iProvider, iCryptoPair) with the 2 parameters: iProvider and iCryptoPair.  
Examples:  
* GET_CRYPTO("gemini";"BTCUSD")  
* GET_CRYPTO("gdax";"ETHUSD")  

Best practices: Try to keep the number of use of the function to keep the number of call to external provider APIs low otherwise they may blacklist your IP.  

### Contribution guidelines ###

[Contribution info](./Documentation/Contribute.md)