/**
 * Verify if a key pair exists on a provider.
 *
 * @param {string} iKey crypto pair to test (BTCUSD, ETHUSD).
 * @param {string} iProviderName provider to use (kraken or gemini).
 * @return true if the keypair exists, False otherwise.
 */
function isKeyValidForProvider(iKey, iProviderName) {
  var aProvider = aCryptoXchanges.filter(function (aOneProvider) {
    return aOneProvider.name == iProviderName;
  });
  if (aProvider.length==1)
  {
    //Logger.log("Pairs are: " + aProvider[0].pair);
    if (iKey in aProvider[0].pair){
      return true;
    }
    else{
      return false;
    }
  }
  else
  {
    //Logger.log("Provider " + iProviderName + " not found");
    return false;
  }
}

/**
 * get a provider.
 *
 * @param {string} iProviderName provider to use (kraken or gemini).
 * @return the provider.
 */
function getProvider(iProviderName) {
  var aProvider = aCryptoXchanges.filter(function (aOneProvider) {
    return aOneProvider.name == iProviderName;
  });
  if (aProvider.length==1)
  {
    return aProvider[0];
  }
  else
  {
    //Logger.log("Provider " + iProviderName + " not found");
    return false;
  }
}