# How to contribute  

# Tests
There is a test function that call various functions to ensure they works fine. You can run it from google script online IDE:

![test function](Test1.PNG)

Then check the logs (Ctrl + Enter or "view logs" in the menu):

![logs from test function run](Test2.PNG)